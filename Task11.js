const DATA = [
    [1,2,3,4],
    [5,18,0,12],
    [3,5,12,5],
    [28,9,2,34]
];

function maxArray(data){
    let maxArr = [];
    for(let i = 0; i<data.length; i++){
        data[i] = data[i].sort((a,b)=>{
            if(a > b){return 1;}
            if(a < b){return -1;}
        });
        maxArr.push(data[i][data[i].length-1]);
    }

    return maxArr;


}

console.log(maxArray(DATA));


function maxArray2(data){
    let maxArr = [];
    for(let i = 0; i<data.length; i++) {
        maxArr.push(data[i].reduce((total, elem) => {
            if(elem > total){
                total = elem;
            }
            return total;

            }, 0)
        )
    }
    return maxArr;
}

console.log(maxArray2(DATA));
