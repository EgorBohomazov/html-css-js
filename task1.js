let users = [
    {
        name: "z",
        surname: "a"
    },
    {
        name: "a",
        surname: "z"
    },
    {
        name: "b",
        surname: "b"
    }
];

let byField = function (field) {
    return function (x, y) {
        if (x[field] > y[field]) {
            return 1;
        }
        if (x[field] < y[field]) {
            return -1;
        }

    }
};

users.sort(byField('name'));
console.log(users);











