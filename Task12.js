const object1 = { a: 1, b: 2, c: { a: 11, b: 3, c: 3 } };
const object2 = { a: 1, c: { a: 1, c: 4, d: 5}, d: 5};

function compareObjects(objFirst, objChanged){
    const result = {};
    result.added = {};
    result.changed = {};
    result.deleted = {};
    for(let key in objChanged){
        if(objFirst[key] === undefined){
            result.added = {};
            result.added[key] = objChanged[key];
        }
        if(objFirst[key] !== undefined && objFirst[key] !== objChanged[key]){
             if(typeof (objChanged[key] == 'object' ) && typeof (objFirst[key]) == 'object'){
                 result.changed = compareObjects(objFirst[key], objChanged[key]);
             }
             else{
            result.changed[key] = objChanged[key];}
        }
    }
    for(let key in objFirst){
        if(objChanged[key] === undefined){
            result.deleted[key] = objFirst[key];
        }
    }


    return result;
}

const result = compareObjects(object1, object2);

console.log(result);