const files = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function uploadFile(file) {
    return new Promise((resolve, reject) => setTimeout(() => {
        try {
            if (Math.random() > 0.5) {
                throw new Error('Failed to upload file');
            }
            console.log(file);
            resolve();
        } catch (e) {
            reject({ error: e, file: file });
        }
    }, parseInt(Math.random() * 300)));

}



async function sortAnswer(files){
  for(let i=0; i<files.length; i++){
      try {
          await uploadFile(files[i]);
      }
      catch (e) {
          console.log(e.file + ' ' + e.error);

      }
  }


}


sortAnswer(files).finally(()=>console.log('done'));








