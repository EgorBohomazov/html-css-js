let result = 0;
function sequence(start,step) {
    result = start;
    return function(){
        result += step;
        return result;
    }

}

const generator = sequence(1,2);

console.log(generator());
console.log(generator());
console.log(generator());

