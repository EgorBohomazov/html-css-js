let someString = 'Some text is here';

function reverse1(str) {
    str = str.split('', str.length);
    str.reverse();
    return str.join('');
}

function reverse2(str){
    let result = '';
    for (let i=str.length-1;i>=0; i--){
        result += str[i];
    }
    return result;
}



console.log(reverse1(someString));
console.log(reverse2(someString));

