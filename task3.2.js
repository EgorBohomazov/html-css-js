
let result = 0;

function sequence(start,step) {
    result = start;
    return function(){
        result += step;
        return result;
    }
}

const generator = sequence(1,2);

function take(func, num){
    let arr = [];
    for(let i=0; i<num; i++){
        arr.push(func());
    }
    console.log(arr);
}

take(generator, 5)



