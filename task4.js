function getData() {
    const ARR = new Array(1000);

    for (let i = 0; i < 1000; i++) {
        ARR[i] = { index: `${parseInt(Math.random() * 10)}.${parseInt(Math.random() * 20)}-${parseInt(Math.random() * 100)}` };
    }
    return ARR;
}



const DATA = getData();
DATA.forEach(elem=>{
    elem.index = elem.index.split('-', 2);
    elem.index[0] = elem.index[0].split('.', 2)
});
let result = DATA.sort(function(x,y){
    if (parseInt(x.index[0][0]) > parseInt(y.index[0][0])) {
        return 1;
    }
    if (parseInt(x.index[0][0]) < parseInt(y.index[0][0])) {
        return -1;
    }
    if (parseInt(x.index[0][1]) > parseInt(y.index[0][1])) {
        return 1;
    }
    if (parseInt(x.index[0][1]) < parseInt(y.index[0][1])) {
        return -1;
    }
    if (parseInt(x.index[1]) > parseInt(y.index[1])) {
        return 1;
    }
    if (parseInt(x.index[1]) < parseInt(y.index[1])) {
        return -1;
    }
});
result.forEach(elem=>{
    elem.index[0] = elem.index[0].join('.');
    elem.index = elem.index.join('-');
});
console.log(result);








