let regExp = /^(http:\/\/|https:\/\/)[a-zA-Z0-9]+(\.com$|\.com\/|\.com\.ua$|\.com\.ua\/|\.ru$|\.ru\/|\.net$|\.net\/|\.ua$|\.ua\/)/;


let str1 = 'http://example.com';
let str2 =  'https://example.com';
let str3 = 'https://example.com/';
let str4 = 'example.com';
let str5 = 'example';
let str6 = ':/123@test,com';

console.log(regExp.test(str1));
console.log(regExp.test(str2));
console.log(regExp.test(str3));
console.log(regExp.test(str4));
console.log(regExp.test(str5));
console.log(regExp.test(str6));


