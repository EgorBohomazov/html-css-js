const DATA = {
    fieldOne: {
        fieldTwo: {
            fieldThree: () => console.log("test")
        },
        fieldFour: 'test',
        someList: [{ test: 'test'  }]
    },
    testField: 'test'
};


function deepCopyObject(obj){
    const copyObj = {};
    for (let key in obj){
        if(typeof (obj[key]) == 'object' && obj[key].length !== undefined){
            const temp = [];
            for(let index in deepCopyObject(obj[key])){
                temp.push(deepCopyObject(obj[key])[index])
            }
            copyObj[key] = temp;
        }
        else if(typeof(obj[key]) == 'object'){
            copyObj[key] = deepCopyObject(obj[key]);
        }
        else{
            copyObj[key] = obj[key]
        }
    }
    return copyObj;

}

result = deepCopyObject(DATA);

result.fieldOne.someList[0].test = 'changed';
console.log(DATA);
console.log(result);











