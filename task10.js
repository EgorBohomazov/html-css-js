let someString = 'Some, text is here...';

function longestWord1(str){
    str = str.split(/[' ' '.'',''!''?']/);
    str = str.sort((a,b)=>{
        if(a.length > b.length){
            return 1;
        }
        if(a.length < b.length){
            return -1;
        }
    });
    return str[str.length-1]
}

console.log(longestWord1(someString));


function longestWord2(str){
    str = str.split(/[' ' '.'',''!''?']/);
    let theLongest = str.reduce((total,elem)=>{
        if(elem.length > total.length){
            total = elem;
        }
        return total
    }, '');

    console.log(theLongest);

}

longestWord2(someString);

