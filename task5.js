const DATA = [
    {
        id: 1,
        parent: null
    },
    {
        id: 2,
        parent: 1
    },
    {
        id: 3,
        parent: 1
    },
    {
        id: 4,
        parent: 1
    },
    {
        id: 5,
        parent: 2
    },
    {
        id: 6,
        parent: 2
    },
    {
        id: 7,
        parent: 3
    },
    {
        id: 8,
        parent: 3
    },
    {
        id: 9,
        parent: 3
    },
    {
        id: 10,
        parent:4
    }
];


function formTreeData (data) {

    const highElem = {};

    data.forEach(elem => {
        if (elem.parent === null) {
            highElem.id = elem.id;
            highElem.parent = elem.parent;
            highElem.children = [];
        }

    });
    data.forEach(elem => {
        if (elem.parent === highElem.id) {
            highElem.children.push(elem);
            elem.children = [];
            data.forEach(elem2 => {
                if (elem2.parent === elem.id) {
                    elem.children.push(elem2);
                }
            })
        }
    });
    return highElem;
}

console.log(formTreeData(DATA));

